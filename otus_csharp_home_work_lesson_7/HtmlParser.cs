﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace otus_csharp_home_work_lesson_7
{
    public static class HtmlParser
    {
        public static List<string> GetAllLinks(string[] urls)
        {
            var links = new List<string>();

            foreach (var url in urls)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader readStream;
                    using Stream receiveStream = response.GetResponseStream();
                    {
                        readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    }
                    string htmlContent = readStream.ReadToEnd();

                    links.AddRange(GetParsedLinks(htmlContent));

                    response.Close();
                    readStream.Close();
                }
            }

            return links;
        }

        private static List<string> GetParsedLinks(string htmlContent)
        {
            /*
            <a href="https://otus.ru/one class="ewrewr"> 1 </a>
            <a href="https://otus.ru/two">2</a>
            <a href="https://otus.ru/three">3</a>
            <a href="https://otus.ru/four">4</a>
            <a href="https://otUS.ru/five">5</a>
            <a href=https://otUS.ru/five>5</a>
            <a href='https://otUS.ru/five'>5</a>
            <a href= httpS://otUS.ru/five >5</a>
            <a href= HTtps://otUS.ru/five >5</a>
            "https://otUS.ru/five?id=1 "
            www.google-assadsad.com

            ptth
            a class="service" href="https://qna.habr.com?utm_source=habr&utm_medium=habr_top_panel">

            <script>Raven.config('https://830576edd4b7478086093f693a5a0df5@s.tmtm.ru/37', {

            whitelistUrls: [/https?:\/\/((www)\.)?(m\.)?habr\.com/],
             */

            var links = new List<string>();

            Regex regex = new Regex(@"([hH][tT]{2}[pP]?[sS]:\/\/[-%@_&a-zA-Z.\/?=\d]*)|([wW]{3}[.]{1}[-%@_&a-zA-Z.\/?=\d]*)");
            MatchCollection matches = regex.Matches(htmlContent);

            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    links.Add(match.Value);
            }
            
            return links;
        }
    }
}