﻿using System;

namespace otus_csharp_home_work_lesson_7
{
    class Program
    {
        static void Main(string[] args)  // args передается в свойствах проекта
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Не указаны входные параметры!");
                return;
            }

            var links = HtmlParser.GetAllLinks(args);

            foreach(var link in links)
                Console.WriteLine(link);

            Console.WriteLine("Нажмите любую клавишу");
            Console.Read();
        }
    }
}
